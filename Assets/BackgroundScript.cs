﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundScript : MonoBehaviour
{
    private float screenWidth;

    void Start()
    {
        
        //Rigidbody2D rigid = GetComponent<Rigidbody2D>();

        //SpriteRenderer  renderer  =  GetComponent<SpriteRenderer>();

        //tamamnho da imagem
        //float imagemLargura = renderer.sprite.bounds.size.x;
        //float imagemAltura = renderer.sprite.bounds.size.z;

        //Tamanho tela 
        //float telaAltura = Camera.main.orthographicSize * 2;
        //telaLargura = telaAltura / Screen.height * Screen.width;

        //Transformando imagem no tamnho da tela
        //Vector2 newScale = this.transform.localScale;
        //newScale.x = telaLargura / imagemLargura + 0.25f;
        //newScale.y = telaAltura / imagemAltura;
        //this.transform.localScale = newScale;

        Rigidbody2D body = GetComponent<Rigidbody2D>();
        //pegando o tamnho da imagem
        SpriteRenderer renderer = GetComponent<SpriteRenderer>();
        float imageWidth = renderer.sprite.bounds.size.x;
        float imageHeight = renderer.sprite.bounds.size.y;

        //pegando o tamanho da tela
        float screenHeight = Camera.main.orthographicSize * 2;
        screenWidth = screenHeight / Screen.height * Screen.width;

        //rescalando o tamanho da imagem no tamanho da tela
        Vector2 newScale = this.transform.localScale;
        newScale.x = screenWidth / imageWidth + 0.25f;
        newScale.y = screenHeight / imageHeight;
        this.transform.localScale = newScale;
    
    }

    // Update is called once per frame
    void Update()
    {
      
    }
}
