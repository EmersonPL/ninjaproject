using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NinjaScript : MonoBehaviour
{
    public float velocity;

    private Rigidbody2D body;

    private SpriteRenderer render;

    private Animator animator;

    
    void Start()
    {
        this.body = this.GetComponent<Rigidbody2D>();
        this.render = this.GetComponent<SpriteRenderer>();
        this.animator = this.GetComponent<Animator>();
        this.animator.enabled = false;
    }

    
    void Update()
    {   
        float x = this.transform.position.x;
        float y = this.transform.position.y;
        float  InputKeyHori = Input.GetAxis("Horizontal");
        if (InputKeyHori > 0)
        {
            this.body.AddForce(new Vector2(this.velocity, 0f));
            this.render.flipX = false;
            this.animator.enabled = true;
        }
        else if (InputKeyHori < 0)
        {
            this.body.AddForce(new Vector2(-this.velocity, 0f));
            this.render.flipX = true;
            this.animator.enabled = true;
        }
        else
        {
            this.body.MovePosition(new Vector2(x, y));
            this.animator.enabled = false;

        }
    }
}
